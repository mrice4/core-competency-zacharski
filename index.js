const express = require("express");
const bodyParser = require("body-parser");
const app = express();
const dateFormat = require("dateformat");

//app.set("port", 8080);
app.set("port", 3000)
app.use(bodyParser.json({type:"application/json"}));
app.use(bodyParser.urlencoded({extended: true}));

const Pool = require("pg").Pool;
const config = {
  host: "localhost",
  user: "[uname]",
  password: "Th3W0rmTurn5",
  database: "[database]"
};

const pool = new Pool(config);

app.get("/[getRequest]", async(req, res) => {
  const q = req.query.q;
  try {

      const template = "SELECT * FROM users";
      const response = await pool.query(template, []);

      if (response.rowCount == 0){
        res.json({error: 'not found' + q});
      }

      else {
        res.json({results: response.rows});
      }

  } catch (err) {
    console.log(err);
    res.json({error: err, status: 'Unforseen error occured, contact builder'});
  }
});

app.post("/[postRequest]]", async(req, res) => {

  const a = req.body.a;
  const b = req.body.b;
  const c = req.body.c;
  const d = req.body.d;

  try {

    const template = "INSERT INTO [database] (a, b, c) VALUES ($1, $2, $3)";
    const response = await pool.query(template, [a, b, c]);
    res.json({status: 'successfully added'});

  } catch (err) {

    console.error("Error Running Post Request: " + err + " code: " + err.code);

    if (err.code == 23502) {
      res.json({status: 'parameters not given'});
    }

    else if (err.code == 23505) {
    res.json({status: 'duplicate entry'});
    }

    else {
      res.json({error: err, status: 'Unforseen error occured, contact builder'});
    }
  }
});

app.listen(app.get("port"), () => {
  console.log(`Find the server at http://localhost:${app.get("port")}`);
});
